#!/usr/bin/env python3

import argparse
import gitlab
from gitlab.exceptions import GitlabAuthenticationError
import json
import os
import requests

TRUSTED_REGISTRATION_PROJECT_ID = 339
TRUSTED_TAGS = ['trusted']
TRUSTED_ACCESS_LEVEL = 'ref_protected' # run jobs from protected branches/tags only

# some colors for diffs
class bcolors:
    OKGREEN = '\033[32m'
    FAIL = '\033[31m'
    ENDC = '\033[0m'

def init_gitlab(instance):
    """ Initializes gitlab api object

    Parameters:
        instance (str): hostname of GitLab instance
    """
    global gl
    gl = gitlab.Gitlab('https://' + instance, private_token=os.environ['GITLAB_TOKEN'])
    gl.auth()

def get_projects_for_runner(id):
    """ Returns all projects for a given Runner. This is not implemented in official gitlab api, so
     graphql api is used. Test querys at https://gitlab.wikimedia.org/-/graphql-explorer.

    Parameters:
        id (str): id of runner

    Raises:
        Exception: Raises exception if GitLab GraphQL API is not responding with http 200

    Returns:
        projects (list(str)): list of project ids (str)
    """
    graphQLQuery = '{runner(id: "gid://gitlab/Ci::Runner/' + str(id) + '"){id projects{nodes{id, name}}}}'
    gitlabURI = gl.url + '/api/graphql'
    gitlabHeaders = {"Authorization": "Bearer " + os.environ['GITLAB_TOKEN']}

    projects = []

    request = requests.post(gitlabURI, json={'query': graphQLQuery}, headers=gitlabHeaders)
    if request.status_code == 200:
        result =  request.json()
        if result['data']['runner']['projects'] != None:
            for project in result['data']['runner']['projects']['nodes']:
                # add id only without full graphql path
                projects.append(project['id'].split("/")[-1])
            return projects
        else:
            print("No projects found. Check TRUSTED_REGISTRATION_PROJECT_ID.")
            exit(1)
    else:
        raise Exception(f"Unexpected status code returned: {request.status_code}")

def get_trusted_runners():
    """ Returns all Trusted Runner. Trusted Runners are identified by root registration
     project defined in TRUSTED_REGISTRATION_PROJECT_ID.

     Returns:
         list(gitlab.runner): a list of gitlab.runner objects
    """

    project = gl.projects.get(TRUSTED_REGISTRATION_PROJECT_ID)
    return project.runners.list(type="project_type")

def authorize_project(trusted_projects, command):
    """ Adds Trusted Runners to a list of trusted projects. Also
     removes access to this projects, if project is removed from list
     of trusted projects.

    Parameters:
        trusted_projects: (list(str)):
    """
    trusted_runners = get_trusted_runners()

    # iterate over all Trusted Runners
    for runner in trusted_runners:
        assigned_projects = get_projects_for_runner(runner.id)
        print("\n" + runner.attributes['description'] + ":")

        # check if assigned projects are still in list of trusted projects (projects.yaml), removes access if not
        for assigned_project in assigned_projects:
            if assigned_project not in trusted_projects:
                project = gl.projects.get(assigned_project)
                print(bcolors.FAIL + "-project with id " + str(assigned_project) + " " + project.name_with_namespace + bcolors.ENDC)
                if command == 'apply':
                    project.runners.delete(runner.id)

        # check if runner is authorized for all trusted projects (projects.yaml), grants access if not authorized
        for trusted_project in trusted_projects:
            if trusted_project not in assigned_projects:
                project = gl.projects.get(trusted_project)
                print(bcolors.OKGREEN + "+project with id " + trusted_project + " " + project.name_with_namespace + bcolors.ENDC)
                if command == 'apply':
                    project.runners.create({'runner_id': runner.id})

def set_runner_config():
    """ Update config for Trusted Runners to make sure they use the correct
    combination of settings. Settings are tag_list, access_level and locked status.
    """
    trusted_runners = get_trusted_runners()

    # iterate over all Trusted Runners
    for runner in trusted_runners:
        runner_detail = gl.runners.get(runner.id)
        runner_detail.tag_list = TRUSTED_TAGS
        runner_detail.acceess_level = TRUSTED_ACCESS_LEVEL
        runner_detail.locked = True
        runner_detail.save()
        print("Updated settings for " + runner.attributes['description'])

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--projects", default='projects.json', help="Filename of configuration file for all projects", nargs='?')
    parser.add_argument("--instance", default='gitlab.wikimedia.org', help="hostname of GitLab instance", nargs='?')
    parser.add_argument("command", help="action to perform, either diff, apply, verify-config or set-runner-config",
                        choices=['diff', 'apply', 'verify-config', 'set-runner-config'])
    args = parser.parse_args()

    try:
        projects_file = open(args.projects, 'r')
        trusted_projects = json.loads(projects_file.read())
    except Exception as e:
        raise SystemExit(f"{bcolors.FAIL}Failed to load {args.projects} file:\n{e}{bcolors.ENDC}")

    if args.command == "verify-config":
        print(f"{bcolors.OKGREEN}Configuration loaded successfully{bcolors.ENDC}")
        return

    init_gitlab(args.instance)

    if args.command == "set-runner-config":
        set_runner_config()
        return

    authorize_project(trusted_projects.keys(), args.command)

if __name__ == "__main__":
    main()
